package com.rest.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class ServiceController{

    @GetMapping("getValue")
    public String getValue() {
        return "Hellow World";
    }

}